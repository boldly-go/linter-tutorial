package main

import (
	"golang.org/x/tools/go/analysis/singlechecker"

	"gitlab.com/boldly-go/linter-tutorial/analyzer"
)

func main() {
	singlechecker.Main(analyzer.Analyzer)
}
